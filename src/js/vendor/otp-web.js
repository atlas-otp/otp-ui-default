//
// Fix for NOSPAM email addresses, mark with class="mail"
//
window.onload = function() {
  document.querySelector("a.mail").onclick = function() {
    var href = this.getAttribute("href");
    this.setAttribute("href", href.replace('@NOSPAM.', '@'));
  }
}
