- Took the copy of antora-ui-default and renamed to otp-ui-default
- Merged in the changes made for the antora website as specified in supplemental-ui of docs.antora.org
- Removed the antora seacrh engine and added lunr
- added free part of font-awesome 5.15.2
- Fixed up all items that are OTP related
